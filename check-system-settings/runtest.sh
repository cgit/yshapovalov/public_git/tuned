#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/tuned/check-system-settings
#   Description: Verify command which checks whether current system settings correspond to the set profile.
#   Author: Tereza Cerna <tcerna@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="tuned"
red=`tput setaf 1; tput bold;`
green=`tput setaf 2; tput bold;`
reset=`tput sgr0`

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlImport "tuned/basic"
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        rlServiceStart "tuned"
        tunedProfileBackup        
    rlPhaseEnd

    rlPhaseStartTest "Verify all profiles."
        modprobe nf_conntrack
        cd /usr/lib/tuned
        for PROFILE in `tuned-adm list | grep "\- " | awk '{ print $2 }'`
        do
            rlLog "${green}Actual profile:  $PROFILE${reset}"
            rlRun "tuned-adm profile $PROFILE" 
            sleep 5
            rlRun "tuned-adm active | grep $PROFILE"
            echo > /var/log/tuned/tuned.log; 
            rlRun "tuned-adm verify --ignore-missing" 0 "Current system setting corresponds with selected profile $PROFILE."
            RESULT=$?
            if [ $RESULT == 0 ]; then
                echo "${green}---> Verification: PASS <---${reset}"
            else
                echo "${red}---> Verification: FAIL <--- ${reset}"
            fi
            echo "ERRORS in /var/log/tuned/tuned.log (This errors cannot be problem, only missing HW, etc.):"
            tput setaf 1;
            cat /var/log/tuned/tuned.log | grep ERROR
            tput sgr0
            echo
        done
        cd -
    rlPhaseEnd

    rlPhaseStartTest "Is error found by tuned-adm verify?"
        rlRun "service tuned restart"
	sleep 3
        rlRun "tuned-adm profile virtual-guest"
        rlRun "tuned-adm active"
        rlRun "tuned-adm verify --ignore-missing" 0 "Current system setting corresponds with selected profile $profile."
        rlRun "sysctl vm.swappiness=77"
        rlRun "tuned-adm verify --ignore-missing" 1 "Current system setting doens't correspond with selected profile $profile."
    rlPhaseEnd

    rlPhaseStartCleanup
        tunedProfileRestore
        rlServiceRestore "tuned"
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
