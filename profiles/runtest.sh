#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/tuned/profiles
#   Description: Checks if profile changes are possible
#   Author: Branislav Blaskovic <bblaskov@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="tuned"
LOG_FILE="profile.log"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlImport "tuned/basic"

        rlFileBackup "/etc/rsyslog.conf"
        rlFileBackup --clean "/etc/tuned.conf.bckp"
        rlFileBackup --clean "/etc/sysconfig/ktune.bckp"
        rlFileBackup --clean "/etc/rc.d/"
       
        tunedProfileBackup
        rlServiceStart "tuned"

        # Set active profile to some default one
        if rlIsRHEL 6
        then
            rlRun "tuned-adm profile default"
        else
            rlRun "tuned-adm profile balanced"
        fi

        #rlRun "setenforce 0" 0 "!!! stop selinux !!!"
        RHTS_OPTION_STRONGER_AVC=""
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "touch $LOG_FILE"
        rlRun "ACTIVE=`tuned-adm active | grep "Current active profile:" | sed 's/Current active profile: //'`"

        IFS_BAK=$IFS
        IFS=$'\n'

        # There can be comments in output of 'tuned-adm list', e.g. '- test-profile  - comment'
        for profile in `tuned-adm list | sed -n '/^-/ {s/- \+/ /g;s/\s\+\(\S\+\).*/\1/;p}'`
        do
            rlLog "====== $profile ======"
            rlRun -s "tuned-adm profile \"$profile\" > start.stdout 2>start.stderr"

            # Debug
            rlRun "cat $rlRun_LOG"

            rlAssertNotGrep "FAIL" "$rlRun_LOG"
            rlAssertNotGrep "Invalid profile" "$rlRun_LOG"

            # Add outputs to log file
            rlRun "echo \"[ $profile ]\" >> $LOG_FILE"
            rlRun "cat $rlRun_LOG >> $LOG_FILE"
            sleep 10
        done

        IFS=$IFS_BAK
    rlPhaseEnd

    rlPhaseStartCleanup
        unset RHTS_OPTION_STRONGER_AVC
        #rlRun "setenforce 1" 0 "!!! run selinux !!!"

        rlRun "test $ACTIVE = "off" && tuned-adm off || tuned-adm profile $ACTIVE" 
        tunedProfileRestore
        rlFileRestore
        rlServiceRestore "tuned"
        rlServiceStop "ktune"
        rlBundleLogs "$LOG_FILE"
    rlPhaseEnd

rlJournalPrintText
rlJournalEnd
